import java.io.*;
import java.util.Scanner;

public class pokemon {
	public static void main(String[] args) throws IOException {
		BufferedReader in = null;
		BufferedWriter out = null;
		String line;
		String type = readTypeFromKeyboard();
		
		try {
			in = new BufferedReader(new FileReader("Pokemon.csv"));
			out = new BufferedWriter(new FileWriter(type));
			
			while((line = in.readLine())!= null) {
				Scanner scan = new Scanner(line);
				scan.useDelimiter(",");
				scan.next();
				scan.next();
				String type1 = scan.next();
				String type2 = scan.next();
				scan.close();
				
				if(type1.equalsIgnoreCase(type) || type2.equalsIgnoreCase(type)) {
					out.write(line);
					out.write("\n");
				}
			}
		}finally {
			if(in != null) {
				in.close();
			}
			if(out != null) {
				out.close();
			}
		}
	}

	private static String readTypeFromKeyboard() {
		Scanner input = new Scanner(System.in);
		System.out.println("Write a type of Pokemon");
		String type = input.next();
		input.close();
		return type;
	}

}
